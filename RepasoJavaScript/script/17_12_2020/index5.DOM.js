function crearParrafo(){
    let todasCapas = document.querySelectorAll('div');
    for(let i=0; i<todasCapas.length; i++){
        const capasIndividiual = todasCapas[i]

        const parrafo = document.createElement('p');
        const textoParrafo = document.createTextNode('Hola');

        parrafo.appendChild(textoParrafo);
        capasIndividiual.appendChild(parrafo);
    }
}

function crearh3(){
    let capaTodos = document.querySelectorAll('div');
    for(let i=1; i<capaTodos.length; i++){
        const capaIndividual = capaTodos[i];
        const h3 = document.createElement('h3');
        const texth3 = document.createTextNode('Soy el DIV ' + [i]);
        h3.appendChild(texth3)
        capaIndividual.appendChild(h3);
    }
    crearParrafo();
}

function crearCapa(){
    let articuloTodos = document.querySelectorAll('article');
   
    for(let i=0; i<articuloTodos.length; i++){
        let articuloIndividual = articuloTodos[i];
        articuloIndividual.id = 'articulo'+[i];

        for(let x=0; x<4; x++){
            const capa = document.createElement('div');
            capa.classList.add('capa');
            articuloIndividual.appendChild(capa);
        }
    }
    crearh3()
}

function crearh2(){
    const primerArticulo = document.querySelector('#articulo0')
    const h2Uno = document.createElement('h2');
    const textoUno = document.createTextNode('Soy el ARTICULO 1');
    h2Uno.appendChild(textoUno);
    primerArticulo.appendChild(h2Uno);

    const segundoArticulo = document.querySelector('#articulo1')
    const h2Dos = document.createElement('h2');
    const textoDos = document.createTextNode('Soy el ARTICULO 2');
    h2Dos.appendChild(textoDos);
    segundoArticulo.appendChild(h2Dos);

    crearCapa();
}

function articulo() {
    let articuloTodos = document.querySelectorAll('article');
    for(let i=0; i<articuloTodos.length; i++){
        let articuloIndividual = articuloTodos[i];
        articuloIndividual.id = 'articulo'+[i];
    }
    crearh2()

}

function cabecera(){
    const cabeza = document.querySelector('header');
    const titulo = document.createElement('h1');
    const textoTitulo = document.createTextNode('Soy un HEADER');

    titulo.appendChild(textoTitulo);
    cabeza.appendChild(titulo);

    articulo()
}

window.onload = function () {
    cabecera();
  }
