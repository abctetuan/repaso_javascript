
const metodoPago = 'efectivo';

switch(metodoPago) {
  case 'efectivo':
    console.log('1. Pagar con efectivo')
    break;
  case 'tarjeta':
    console.log('2. Pagar con tarjeta');
    break;
  case 'cheque':
    console.log('3. Pagar con cheque'); 
    break;
  default:
    console.log("4. No has selecionado un método de pago");
    break;
}


