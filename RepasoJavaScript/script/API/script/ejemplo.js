const datos = {
   
    categories: [
      {
        text: "Personajes",
        url: "https://swapi.dev/api/people/",
      },
      {
        text: "Planetas",
        url: "https://swapi.dev/api/planets/",
      },
      {
        text: "Vehiculos",
        url: "https://swapi.dev/api/vehicles/",
      },
  
    ],
  }
  
  
  
  function crearEnlace( linkTxt, miFuncionFavorita) {
      const a = document.createElement('a');
      const link = document.createTextNode(linkTxt);
      a.appendChild(link);
      a.setAttribute('href', '#');

      a.addEventListener('click', function(event) {
        event.preventDefault();
        miFuncionFavorita();
    });

      return a;
  }
  
   function cabecera(){
      const main = document.querySelector('main');
  
      const cabecera = document.createElement('div');
      cabecera.classList.add('col-10');
  
      const imagenCabecera = document.createElement('img');
      imagenCabecera.src = './img/cabecera.png';
  
      cabecera.appendChild(imagenCabecera);
      main.appendChild(cabecera);
  }
  
  
  function navegador(){
    const main = document.querySelector('main');
    const navegador = document.createElement('div');
    navegador.classList.add('col-11');
    main.appendChild(navegador);
   
    for (let i=0; i<=datos.categories.length-1; i++){
        const enlace = crearEnlace(datos.categories[i].text, function () {
            clearContainer();
            crearTitulo(datos.categories[i].text);
            crearTabla(datos.categories[i].url);
        });
        navegador.appendChild(enlace)
    }
  }
  
  
  
  function clearContainer() {
    document.querySelector('article').innerHTML = "";
  }
  
  function contenido(){
      const main = document.querySelector('main');
      const contenido = document.createElement('div');
      contenido.classList.add('col-12');
      main.appendChild(contenido);
      const articulo = document.createElement('article');
      contenido.appendChild(articulo);
  }
    
  function crearTitulo(txt){
    const articulo = document.querySelector('article');
    const titulo = document.createElement('h1');
    const tituloTxt = document.createTextNode(txt);
    titulo.appendChild(tituloTxt);
    articulo.appendChild(titulo);
  }
    
  function crearTabla(url){
  
  fetch(url)
  
  .then(respuesta => respuesta.json())
  
  .then( function (data){
    
      const articulo = document.querySelector('article');
      const list = document.createElement('ul');
  
    
      for (let i=0; i<=data.results.length-2; i++){
      
      const listItem = document.createElement('li');
  
      const aTxt = crearEnlace(data.results[i].name, function() {
          clearContainer();
          crearContenidoSecundario(data.results[i].name); 
          crearTablaSecundaria(data.results[i]);//
        });
        
        listItem.appendChild(aTxt);
        list.appendChild(listItem);
        articulo.appendChild(list);
        }
      })
      .catch( function (errors) {
        console.log(errors);
        });
      
  }
  
  function crearContenidoSecundario(list) {
    const articulo = document.querySelector('article');
    const subTitulo = document.createElement('h1');
    const subTituloTxt = document.createTextNode(list);
    subTitulo.appendChild(subTituloTxt);
    articulo.appendChild(subTitulo);
  }
  
  function crearTablaSecundaria(list){
    const articulo = document.querySelector('article');
    const listDos = document.createElement('ul');
    listDos.classList.add('contenido');
  
    console.log(list);
  
      for (let item in list){
      const txtTres= document.createTextNode(list[item]);
      const txtDos= document.createTextNode(item);
      const parDos= document.createElement('p');
      const parTres= document.createElement('p');
  
      parDos.appendChild(txtDos);
      parTres.appendChild(txtTres);
  
  
      const listItem = document.createElement('li');
      listItem.appendChild(txtDos);
      listItem.appendChild(parTres);
      listDos.appendChild(listItem);
    }
    articulo.appendChild(listDos);
  }
  
  
  window.onload = function () {
      cabecera();  
      navegador();
      contenido();
    };
  