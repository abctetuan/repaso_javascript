



const resulta1 = 20 + 30 * 2;
console.log(resulta1);

const resulta2 = (20 + 30) * 2;
console.log(resulta2);


const resulta3 = 20 + 30 + 40;
console.log(resulta3);

const resulta4 = (20 + 30 + 40) * .21;
console.log(resulta4);

const resulta5 = (20 + 30 + 40) * 1.21;

console.log(resulta5);


let puntuacion = 5;

console.log(puntuacion);

puntuacion++

console.log(puntuacion);

puntuacion++

console.log(puntuacion);

puntuacion--

console.log(puntuacion);

puntuacion--

console.log(puntuacion);

puntuacion +=3

console.log(puntuacion);

puntuacion -=3

console.log(puntuacion);


const numer1 = "20";
const numer2 = "20.2";
const numer3 = "dos";
const numer4 = 22;
const numer5 = 22.5;

// En este caso es una cadena
console.log(numer1);
console.log(typeof numer1);

// En este caso es un número
console.log(numer4);
console.log(typeof numer4);

// En este caso transformamos una cadena en un número
console.log(Number.parseInt(numer1));
console.log(numer1);
console.log(typeof numer1);

// En este caso transformamos una cadena en un número flotante (con decimales)
console.log(numer2);
console.log(Number.parseFloat(numer2));
console.log(numer2);

// En este caso no pueden cambiar a número. NAN
console.log(Number.parseInt(numer3));

// Saber si el número es entero
console.log(Number.isInteger(numer4));
console.log(Number.isInteger(numer5));