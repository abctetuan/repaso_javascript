
const usuarioUno = 'Rodrigo';
const usuarioDos = 'Raquel';


// Función tradicional
function funcionTradicional(user1){
    console.log(user1);
}

funcionTradicional(usuarioUno)


// Función anónia. Esta "metida" dentro de una constante
const funcionAnonima = function (user1){
    console.log(user1);
}

funcionAnonima(usuarioDos)


const usuarioPepe = true;
const usuarioMaria = false;


function cuartafn(user){
    if(user){
        console.log( user + ' te dejo entrar en la zona vip de mi web');
    } else{
        console.log( user + ' No puedes entrar, logueate');
    }
}

cuartafn(usuarioPepe);



const quintafn = function (user){
    if(user){
        console.log( user + ' te dejo entrar en la zona vip de mi web');
    } else{
        console.log( user + ' No puedes entrar, logueate');
    }
}

quintafn(usuarioMaria);