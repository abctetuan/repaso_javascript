/////////////////////

let result;

// Número PI
result = Math.PI;

console.log(result);


// Redondea 
result1 = Math.round(2.8);
result2 = Math.round(2.2);

console.log(result1);
console.log(result2);


// Redondea hacia arriba
result3 = Math.ceil(2.8);
result4 = Math.ceil(2.2);

console.log(result3);
console.log(result4);

// Redondea hacia abajo
result5 = Math.floor(2.8);
result6 = Math.floor(2.2);

console.log(result5);
console.log(result6);

// Raíz cuadrada
result6 = Math.sqrt(100);
console.log(result6);

// Ignorar el negativo
result7 = Math.abs(-500);
console.log(result7);

// potencia
result8 = Math.pow(2, 5);
console.log(result8);

// Conocer el mínimo y el máximo
result9 = Math.min(4, 8);
console.log(result9);

result10 = Math.max(5, 9);
console.log(result10);


// Número random
result11 = Math.random();
console.log(result11);

// Número random dentro de un rango. Va de Cero a Treinta
result12 = Math.floor ( Math.random() * 30 );
console.log(result12);


/////////////////////////////////////