const frutas = ['pera', 'manzana', 'platano', 'naranja', 'platano'];

const posicionFruta = frutas.indexOf('platano');

console.log(posicionFruta);

const posicionFrutaDos = frutas.lastIndexOf('platano');

console.log(posicionFrutaDos);

////

const contieneFruta = frutas.includes('platano');

console.log(contieneFruta);

////

const frutasDos = ['limon', 'sandia', 'kiwi'];

const unirFrutas = frutas.concat(frutasDos);

console.log(unirFrutas);

const unirfrutasDos = [...frutas, ...frutasDos]

console.log(unirFrutas);

///

const frutaUnidad = 'mandarina';

const incluirFruta = frutas.push(frutaUnidad);

console.log(frutas);

//////

const borrarFruta = frutas.pop();

console.log(frutas);

////

const frutaNueva = 'arandanos'

const nuevFrutaInicio = frutas.unshift(frutaNueva)

console.log(frutas);


/////

const frutastres = [ 'manzana', 'platano', 'naranja', 'platano'];

const nuevoArrayFrutas = frutastres.slice(1, 4)

console.log(nuevoArrayFrutas);

/////

const cadenaFrutasDos = frutas.toString();

console.log(cadenaFrutasDos);

/////

const cadenaFrutas = frutas.join( " - " );

console.log(cadenaFrutas);

/////

const darVueltaFrutas = frutas.reverse();

console.log(darVueltaFrutas);

/////

const borrarPrimeraFruta = frutas.shift();

console.log(borrarPrimeraFruta);

////

const ordenarFrutas = frutas.sort();

console.log(ordenarFrutas);

////

const cadena = 'Sandias y melones son para el verano';

const nuevoArray = cadena.split();

console.log(nuevoArray);

//////

const frutaTropical = 'Piña';

console.log(frutaTropical.replace('Piña', 'Manzana'));



/////

const nuevoArrayFrutasDos = frutas.slice(0, 1)

console.log(nuevoArrayFrutasDos);

/////


const nuevoArrayFrutasTres = frutas.splice(2, 1, 'piña')

console.log(frutas);