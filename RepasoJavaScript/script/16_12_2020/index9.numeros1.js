

const array = [10, 20, 35]
let nuevoArray = [];

for (let i = 0 ; i< array.length; i++){

    const num = array[i];
    if (num % 2 === 0){
        nuevoArray.push(num);  
    } 
}

console.log(nuevoArray);

const numero1 = 20;
const numero2 = "20";

console.log(numero1);
console.log(numero2);

const num1 = 20;
const num2 = 30;


const num3 = 30.30;
const num4 = .30;
const num5 = -30;

console.log(num1);
console.log(num2);

console.log(num3);
console.log(num4);
console.log(num5);

const num6 = new Number(40);

console.log(num6);
