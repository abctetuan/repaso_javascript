const array3 = ['Volvo', 'Ford', 'Renault'];

const objeto1 = 
    {
        nombre: 'Ana',
        edad: 20,
        mujer: true,
    }



for (let i = 0; i < array3.length; i++){
    console.log(array3[i]);
}


for (let item of array3) {
    console.log(item);
}


for (const key in objeto1) {
     console.log(objeto1[key]);
}


const alien = {
    name: 'Wormuck',
    race: 'Cucusumusu',
    planet: 'Eden',
    weight: '259kg',
}

for (const key in alien) {
    console.log(alien[key]);
    console.log(key);
    console.log('Propiedad: ' + key + ', valor: ' + alien[key])
}