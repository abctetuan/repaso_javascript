

const usuarioUno = 'Rodrigo';
const usuarioDos = 'Raquel';

// Arrow function o función flecha. Esta "metida" dentro de una constante


const funcionFlecha1 = user1 => {
console.log(user1);
}

funcionFlecha1(usuarioUno)

const funcionFlecha2 = (user1, user2) => {
    console.log(user1, user2);
}

funcionFlecha2(usuarioUno, usuarioDos)


const funcionFlecha3 = user1 => {
    console.log(user1);
}

funcionFlecha3(usuarioDos)


const funcionFlecha4 = user1 => console.log(user1);

funcionFlecha4(usuarioDos)



